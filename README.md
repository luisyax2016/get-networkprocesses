# Get-NetWorkProcesses

Function Get-NetworkProcesses 
.Synopsis
   Gets a list of active network processes the netstat way, including username and commandline argument running active network process.

.DESCRIPTION
   Adding native PowerShell comandlets using Get-NetTCPConnection, Get-WmiObject, and Get-Process to get a current list of network processes with UserName and actual command executing process along with network info the Netstat way.

   Collumns returned:
   LocalAddress
   LocalPort
   RemoteAddress
   State
   OwningProcess
   UserName
   CommandLine

.EXAMPLE
   Get-NetworkProcesses

.EXAMPLE
   Get-NetworkProcesses | Where-Object {$_.State -like '*bound*'} | ft -AutoSize

To try it out:

$uri = "https://gitlab.com/luisyax2016/get-networkprocesses/-/raw/main/get-networkprocesses.ps1";
iex ((New-Object System.Net.WebClient).DownloadString($uri))


