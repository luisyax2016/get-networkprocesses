Function Get-NetworkProcesses {
<#
.Synopsis
   Gets a list of active network processes the netstat way, including username and commandline argument running active network process.
.DESCRIPTION
   Adding native PowerShell comandlets using Get-NetTCPConnection, Get-WmiObject, and Get-Process to get a current list of network processes with UserName and actual command executing process along with network info the Netstat way.
   Collumns returned:
   LocalAddress
   LocalPort
   RemoteAddress
   State
   OwningProcess
   UserName
   CommandLine
.EXAMPLE
   Get-NetworkProcesses
.EXAMPLE
   Get-NetworkProcesses | Where-Object {$_.State -like '*bound*'} | ft -AutoSize
#>
	BEGIN {
		$NetworkProcesses = Get-NetTCPConnection | Select-Object LocalAddress, LocalPort, RemoteAddress, State, OwningProcess | ForEach-Object {
			$localaddress    = $_.LocalAddress
			$localport       = $_.LocalPort
			$remoteaddress   = $_.RemoteAddress
			$state           = $_.State
			$owningprocessid = $_.OwningProcess; $username = (Get-Process -Id $owningprocessid -IncludeUserName).UserName
			$commadline      = (Get-WmiObject -Class win32_process | Where-Object {$_.ProcessId -eq $owningprocessid}).CommandLine
			$Result          = [PSCustomObject]@{
			LocalAddress     = $localaddress
			LocalPort        = $localport
			RemoteAddress    = $remoteaddress
			State            = $state
			OwningProcess    = $owningprocessid
			UserName         = $username
			CommandLine      = $commadline
			}
		$Result
		}
	}
	PROCESS {

	}
	END {
		$NetworkProcesses
	}
}
